UPDATE: Added new file to deal directly with spreadsheets and use command line arguments (checkAllCartridgeSpreasheetLinks_FinalPreCartridge.rb)
Notes on that:

On the command line run the ruby program with the following command:
ruby checkAllCartridgeSpeadsheetLinks_FinalPreCartridge.rb --bfp=full/path/to/the/spreadsheets
Note: If the directories containing the files are not in the base directory (ex. Integrated and CA have subdirectories ("CA", "Integrated") that contain the folders with files then you need to pass an additional argument --rfp=full/path/to/the/resources
Also note: The folders with the resource files are expected to match the first column in the spreadsheets

If you want to test only a specific spreadsheet, --file=filename.xlsm can be used.
If you want to only build the structure and not test the links --structOnly=true can be used

Just running the program without arguments will list the parameters that can be passed in and then abort

What this will do:
example: ruby checkAllCartridgeSpreadsheetLinks_FinalPreCartridge.rb --bfp=T:\technology\common_cartridge\Integrated --rfp=T:\technology\common_cartridge\Integrated\Integrated

if you do not specify a file with "--file=filename", for each *.xl* file it will convert the first two sheets to csv files and put them in T:\technology\common_cartridge\Integrated\Testing\Csv folder with a timestamp in the name (Note: Ideally, these csvs would not be necessary, but originally I was doing this manually and I didn't want to rewrite much of the program...and I figured a csv record/backup might be nice to have for some reason)

It will then parse those csv files for structure and build timestamped html files in nested list format to represent the cartridge and put those files in T:\technology\common_cartridge\Integrated\Testing\Structure - This will have buttons to see only a specific type of resource, based on the resources that are found in the given cartridge. This will help to identify any sections missing something

If you did not specify "--structOnly=true", it will then check every file/link and put two timestamped html files in T:\technology\common_cartridge\Integrated\Testing\LinkCheck -One html file is a list of all links/files, the other is a list of ones that could not be verified for one reason or another.
As it is checking links, it will write a count out on the cmd screen to indicate its place in the job and that the process is still occurring.

A few additional notes:
* because we do not have a consistent and reliable course resource structure, the structure files must each be manually inspected to verify everything is in order
* urls that do not use "https" are treated as files and will be treated as not found to help identify the error
* Files are verified significantly faster than web links (naturally), so the program will show bursts of moving quickly and then appear to be running slow, depending on the grouping of links
* The html files have buttons to help filter the info. These (specifically the link files) will only work if the scripts completes (if you do not interrupt the run)


Testing Cartridge after it is made into an imscc file
unzip the file, and in a similar fashion to the workflow above, run convertCartridgeXMLToStructureAndLinks.rb --bfp=path/to/extracted/cartridge
This path should point to the directory that contains imsmanifest.xml and a data directory
This xml will be parsed and open other xml files to pull together all links and files and then they will be tested and the structure will be output.




OLD NOTES ON PREVIOUS (OUTDATED) RUBY SCRIPT IN THIS REPO:

This is a quick and dirty script intended to help with the QAing of common cartridges. It will parse links and check them while also creating an html file with lists to replicate the expected structure of the cartridge.

The rb script itself will need updated for different runs (set the paths etc), but this should be a good starting point.

Example workflow:

Copy the Common Cartridge Directory (this should include all xlsm files and a directory with the linked files for the cartridge)
Export each teacher and student sheet from each workbook to csv in a "Testing" subdirectory(eg. common_cartridge_cc_adv1.xlsm => Testing/common_cartridge_cc_adv1_teacher.csv and Testing/common_cartridge_cc_adv1_student.csv)
Copy checkAllCartrigeCSVLinks_FinalPreCartrige.rb into the Parent Directory of the Directory with the original common cartridge files.
Look over the checkAllCartrigeCSVLinks_FinalPreCartrige.rb file to make sure it fits the inputs (check the "baseDir" variable to make sure it is pointing at the common cartridge directory you copied to).
run the ruby file (checkAllCartrigeCSVLinks_FinalPreCartrige.rb), this will take each csv in the testing directory, check to make sure the files/links are valid and create several files
1. a file of all links checked 
2. a file of all errors encountered
3. a file with the cartridge structure for each csv

Scroll through the error file to identify areas that need corrected
Look over the structure files to identify any missing pieces or oddities (ie. California links for Common Core)

example directory structure
* LinkChecker(folder)
** checkAllCartrigeCSVLinks_FinalPreCartrige.rb
***MiddleSchool_B2S_CC(folder)
***Adv1(folder containing all files to be included in program cartridges)
***Adv2(folder containing all files to be included in program cartridges)
***Red(folder containing all files to be included in program cartridges)
***Green(folder containing all files to be included in program cartridges)
***Blue(folder containing all files to be included in program cartridges)
***Alg1(folder containing all files to be included in program cartridges)
***Accel(folder containing all files to be included in program cartridges)
***Testing(folder)
****all csvs for the programs in the parent folder (both student and teacher)
****LinkCheck(folder)
*****This is the output from the ruby script