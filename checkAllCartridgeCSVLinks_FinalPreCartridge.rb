require 'rubygems'
require 'xmlsimple'
require 'net/https' #use https or http depending on use- also see code below
require 'openssl'
require 'csv'

# 00 name
# 01 program
# 02 Chapter
# 03 Chapter Title
# 04 Section
# 05 Standard(s)
# 06 Section Title
# 07 Resource Title
# 08 Resource File/Path/URL
# 09 Resource Type
# 14 Unique Title
# 15 
# 16 Guid 1
# 17 Guid 2
# 18 Guid 3
# 19 Guid 4
# 20 Guid 5
# 21 Guid List Concat
# 22 Guid List
# 23 Role

Entry = Struct.new(:name, :program, :chapter, :chapTitle, :section, :standards, :sectionTitle, :resourceTitle, :resourcePath, :resourceType, :uniqueTitle)
$brokenLinks = 0
$allbrokenLinks = 0
$allLinks = 0
baseDir = 'C:/Users/jklins/Documents/sandbox/CommonCartridgeLinkChecker/MiddleSchool_B2S_CC/'
timeInt = Time.now.to_i
fileoutName = "#{baseDir}Testing/LinkCheck/allLinksChecked_#{timeInt}.html" 
fileoutNameNotFound = "#{baseDir}Testing/LinkCheck/allLinksNotFound_#{timeInt}.html"
fixIncompleteUrls = false #use to handle links that start with things like "/protected/....."
#inputCSV = "allBookMaterialLinksUnchanged.csv" #"allBookMaterialLinksUnchanged.csv" #"recheckLinks.csv"
startLine = 1

$jquery = "<script src='https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js'></script>"
$buttons = "<div><button id='AllLines'>All</button><button id='NotFound'>Not Found</button><button id='SomethingWentWrong'>Something went wrong</button><div><br/>"
$js = "<script>$('#AllLines').click(function (){$('.all').show();}); $('#NotFound').click(function (){$('.all').hide();$('.notFound').show();}); $('#SomethingWentWrong').click(function (){$('.all').hide();$('.somethingWentWrong').show()});</script>"

def CheckLink(origString, urlString, limit, line, fileout, fileoutNotFound, getRequest = false)
	begin	
		url = URI.parse(urlString)
		http = Net::HTTP.new(url.host, url.port)
		if (urlString.index("https") == 0)	
				http.use_ssl = true
				http.verify_mode = OpenSSL::SSL::VERIFY_NONE
		end
		if (getRequest == false) #This if/else is the best I could do to handle redirects and 404s :( ...There is a better way, I'm sure of it!
			request = Net::HTTP::Head.new(url.request_uri)
			request['Cookie'] = "loggedin=1,teacher=1"
			r = http.request(request)
		else
			r = http.get(url.request_uri)
			cookie = {'Cookie'=>r.to_hash['set-cookie'].collect{|ea|ea[/^.*?;/]}.join}
			r = http.get(url.request_uri,cookie)
		end
	rescue
		p "something went wrong with this request: #{$!.backtrace}  #{line}: #{urlString}"
		fileoutNotFound.puts "<div class = 'all somethingWentWrong'>something went wrong with this request (rescued): | Line: #{line} |<a href ='#{origString}'>#{origString}</a>| #{urlString} | #{$!.backtrace} <br/></div>"
		$brokenLinks = $brokenLinks + 1
		$allbrokenLinks = $allbrokenLinks + 1
		return
	end
	case r
		when Net::HTTPSuccess     then 
			print "#{line}, "
		when Net::HTTPRedirection then 
			#p "redirected  Line: #{line} #{origString}- > #{r['location']}"
			if((r['location'] == '/BIM/login') and (limit > 0))
				CheckLink(origString, origString, limit - 1, line, fileout, fileoutNotFound, true)
			else
				CheckLink(origString, r['location'], limit - 1, line, fileout, fileoutNotFound)
			end
		when Net::HTTPNotFound
			$brokenLinks = $brokenLinks + 1
			$allbrokenLinks = $allbrokenLinks + 1
			fileoutNotFound.puts "<div class='all notFound'>failed checking -NOT FOUND - (#{r.code}) | Line: #{line} |<a href ='#{origString}'>#{origString}</a>| |<a href =#{urlString}>#{urlString}</a><br/></div>"
			p "failed checking -NOT FOUND - (#{r.code})- Line: #{line} | #{origString}"
		else
		$brokenLinks = $brokenLinks + 1
		$allbrokenLinks = $allbrokenLinks + 1
		fileoutNotFound.puts "<div class = 'all somethingWentWrong'>failed checking (#{r.code})- | Line: #{line} |<a href ='#{origString}'>#{origString}</a>| |<a href =#{urlString}>#{urlString}</a><br/></div>"
	end
end
 

def buildStructure(item) 
	contentType = Hash.new();
	chapters = 0
	sections = 0
	i = -1
	book = Array.new()
	#note: if using the June 2015 excel spreadsheets, the line breaks in the headers must first be removed
	$currentChap = ""
	$currentBook = ""
	$counter = 0
	time =  Time.now.to_i
	fileoutName = "#{item.chomp('.csv')}_structure#{time}.html"  #output file name
	fileoutName.sub! '/Testing/', '/Testing/LinkCheck/'
	fileout2 = File.open(fileoutName, "w")
	CSV.foreach(item) do |currentRow|
		i += 1
		if i == 0 
			next
		end
		cType = currentRow[9].nil? ? "unknownType" : currentRow[9].gsub(/\s+/, "_").downcase.delete("( ):/!\t").to_sym
		if (contentType[cType].nil?)
			contentType[cType] = 1
		else 
			contentType[cType] = contentType[cType] + 1
		end
		book.push(Entry.new(currentRow[0],currentRow[1],currentRow[2].to_i,currentRow[3],(currentRow[4].nil? ? "unknownSection" : currentRow[4]),currentRow[5],currentRow[6],currentRow[7],currentRow[8],(currentRow[9].nil? ? "unknownType" : currentRow[9]),currentRow[14]))
	end
	p item
	newbook = book.sort_by{|e| [e.chapter, e.section, e.resourceType]}
	holdEntry = Entry.new
	j = -1
	fileout2.puts "<!DOCTYPE html><head><title>#{item} - Structure </title>#{$jquery}</head><body><h1>#{item} - Structure</h1><ul>"
	newbook.each do |b|
		j += 1
		if b.chapter == 0 and b.resourceType == "" and b.resourcePath == ""
			next
		end
		if b.chapter == 0
			fileout2.puts "<li class='all #{b.resourceType.gsub(/\s+/, "_").downcase.delete("( ):/!\t").to_sym}'><span style =\"color:green\">#{b.resourceType}</span> -- <span style=\"color:orange\">#{b.resourceTitle}</span> -- <a href = \"#{b.resourcePath}\">#{b.resourcePath}</a></li>"
			next
		end
		if b.chapter == holdEntry.chapter
			if b.section == holdEntry.section
				fileout2.puts "<li class='all #{b.resourceType.gsub(/\s+/, "_").downcase.delete("( ):/!\t").to_sym}'><span style =\"color:green\">#{b.resourceType}</span> -- <span style=\"color:orange\">#{b.resourceTitle}</span> -- <a href = \"#{b.resourcePath}\">#{b.resourcePath}</a></li>"
			else
				sections = sections+1
				fileout2.puts "</ul></ul>"
				fileout2.puts "<ul><li><h3>#{b.section}<h3></li><ul>"
				fileout2.puts "<li class='all #{b.resourceType.gsub(/\s+/, "_").downcase.delete("( ):/!\t").to_sym}'><span style =\"color:green\">#{b.resourceType}</span> -- <span style=\"color:orange\">#{b.resourceTitle}</span> -- <a href = \"#{b.resourcePath}\">#{b.resourcePath}</a></li>"
			end
		else
			if j != 0
			fileout2.puts "</ul></ul>"
			end
			chapters = chapters+1
			sections = sections+1
			fileout2.puts "<li><h2>#{b.chapter}</h2></li>"
			fileout2.puts "<ul><li><h3>#{b.section}</h3><ul><li class='all #{b.resourceType.gsub(/\s+/, "_").downcase.delete("( ):/!\t").to_sym}'><span style =\"color:green\">#{b.resourceType}</span> -- <span style=\"color:orange\">#{b.resourceTitle}</span> -- <a href = \"#{b.resourcePath}\">#{b.resourcePath}</a></li>"
		end
		holdEntry = b
	end
	contentJs="$('#ALL').click(function(){$('.all').show();});\r\n"
	buttons = "<button id='ALL'>all</button>"
	contentType.each do |k,v|
		contentJs = "#{contentJs} $('##{k.upcase}').click(function(){$('.all').hide();$('.#{k}').show();});\r\n"
		buttons = "#{buttons} <button id='#{k.upcase}'>#{k} (#{v})</button>"
	end
	buttons = "<h2>Number of Chapters: #{chapters}</h2><h2>Number of Sections: #{sections}</h2> #{buttons}"
	contentJs = "<script>  $( document ).ready(function() { $('body').prepend(\"#{buttons}\"); #{contentJs}});</script>"
	fileout2.puts "</ul></li></ul>#{contentJs}</body></html>"  
	fileout2.close
end
 
def main(baseDir, fileoutName, fileoutNameNotFound, fixIncompleteUrls, startLine)
	fileout = File.open(fileoutName, "w")
	fileout.puts "<!DOCTYPE html><html><head><title>Common Cartridges all Links</title></head><body>"
	fileoutNotFound = File.open(fileoutNameNotFound, "w")
	fileoutNotFound.puts "<!DOCTYPE html><html><head><title>Common Cartridges all Links Not Found</title>#{$jquery}</head><body>#{$buttons}"
	checkedLinks = 0
	webLink = false
	Dir.glob("#{baseDir}Testing/*.csv") do |item|
		buildStructure(item)
		checkedLinks = 0
		fileout.puts "<h1>#{item}</h1>"
		fileoutNotFound.puts "<h1>#{item}</h1>"
		firstLine = true
		CSV.foreach(item) do |row| #Note if using csv from database, quotes needed removed from some row
			if (firstLine == true)
				firstLine = false
				next
			end
			checkedLinks = checkedLinks + 1
			$allLinks = $allLinks + 1
			webLink = false
			if((row[8].to_s.empty?) or (row[0].to_s.empty?))
				fileoutNotFound.puts "empty line:| #{checkedLinks}"
				next
			end
			if (row[8].index("https") == 0) #If it's a webLink or if it's a file. Weblinks for cartidges must start with https, not http or //
				fullUrl = row[8]
				webLink = true
			else
				fullUrl = "#{baseDir}#{row[0].capitalize}/#{row[8]}" #get file full path for checking
			end
			if checkedLinks < startLine
				next
			end
			fileout.puts "Line: #{checkedLinks}: <a href ='#{fullUrl}'>#{fullUrl}</a><br/>"
			if(webLink == true)
				begin
					CheckLink(fullUrl, fullUrl, 10, checkedLinks, fileout, fileoutNotFound)
				rescue
					$brokenLinks = $brokenLinks + 1
					$allbrokenLinks = $allbrokenLinks + 1
					p "failed checking (rescued)- #{item} Line: #{checkedLinks}: | <a href ='#{fullUrl}'>#{fullUrl}</a><br/>"
					fileoutNotFound.puts "<div class = 'all somethingWentWrong'>failed checking (rescued): #{$!.backtrace}  | #{item} Line: #{checkedLinks} |<a href ='#{fullUrl}'>#{fullUrl}</a>|<br/></div>"
				end
			else
				print "#{checkedLinks}, "
				if(File.file?(fullUrl))
					#found the file
				else
					p "#{item} Line: #{checkedLinks}: | #{fullUrl}"
					fileoutNotFound.puts "<div class = 'all somethingWentWrong'>failed checking FILE: #{item} Line: #{checkedLinks} |<a href ='#{fullUrl}'>#{fullUrl}</a>|<br/></div>"
				end
			end
		end
	end


	fileout.puts "<h2>Total Checked Links: #{$allLinks}</h2>"
	fileout.puts "<h2>Total Broken Links: #{$allbrokenLinks}</h2>"
	fileout.puts "See <a href = 'AllCartridgeLinksNotFound.html'>AllCartridgeLinksNotFound.html</a> For Specific Info Related to Links without 200 code returns"
	fileout.puts "</body></html>"
	p "Total Checked Links: #{checkedLinks} File with links: #{fileoutName}"
	p "Total Broken Links: #{$allbrokenLinks} File with links: #{fileoutNameNotFound}"
	fileout.close
	fileoutNotFound.puts "<h2 style='color:blue'>Total Checked Links: #{$allLinks}</h2>"
	fileoutNotFound.puts "<h2 style='color:red'>Total Broken Links: #{$allbrokenLinks}</h2>"
	fileoutNotFound.puts "#{$js}</body></html>"
	fileoutNotFound.close
end 

main(baseDir, fileoutName, fileoutNameNotFound, fixIncompleteUrls, startLine)