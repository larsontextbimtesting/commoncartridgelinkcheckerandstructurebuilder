#make sure we have all the gems we need
begin
  require 'rubygems'
rescue LoadError
  puts "Required gem rubygems not found. Installing now..."
  `gem install rubygems`
  puts "done installing rubygems"
  require 'rubygems'
end
begin
  require 'xmlsimple'
rescue LoadError
  puts "Required gem xmlsimple not found. Installing now..."
  `gem install xmlsimple`
  puts "done installing xmlsimple"
  require 'xmlsimple'
end
begin
  require 'net/https' #use https or http depending on use- also see code below
rescue LoadError
  puts "Required gem net/https not found. Installing now..."
  `gem install net/https`
  puts "done installing net/https"
  require 'net/https'
end
begin
  require 'openssl'
rescue LoadError
  puts "Required gem openssl not found. Installing now..."
  `gem install openssl`
  puts "done installing openssl"
  require 'openssl'
end
begin
  require 'csv'
rescue LoadError
  puts "Required gem csv not found. Installing now..."
  `gem install csv`
  puts "done installing csv"
  require 'csv'
end
begin
  require 'roo'
rescue LoadError
  puts "Required gem roo not found. Installing now..."
  `gem install roo`
  puts "done installing roo"
  require 'roo'
end
# 00 name
# 01 program
# 02 Chapter
# 03 Chapter Title
# 04 Section
# 05 Standard(s)
# 06 Section Title
# 07 Resource Title
# 08 Resource File/Path/URL
# 09 Resource Type
# 14 Unique Title
# 15 
# 16 Guid 1
# 17 Guid 2
# 18 Guid 3
# 19 Guid 4
# 20 Guid 5
# 21 Guid List Concat
# 22 Guid List
# 23 Role

Entry = Struct.new(:name, :program, :chapter, :chapTitle, :section, :standards, :sectionTitle, :resourceTitle, :resourcePath, :resourceType, :uniqueTitle)
$brokenLinks = 0
$allbrokenLinks = 0
$allLinks = 0
$fileout
$fileoutNotFound
				
##################################################################################################################################
$args = {}

def PrintHelpMessage()
	print "\nplease run the script with the following options:\n"
	print "BASE FILE PATH: (REQ):    '--bfp=base/File/Path/' this is the location of\n"
	print "                                                  the cartridge spreadsheets\n\n"
	print "RESOURCE FILE PATH (OPT): '--rfp=resource/File/Path/' if the folders containing\n"
	print "                                                      resources are not in\n"
	print "                                                      the base file path\n\n"
	print "FILENAME (OPT):           '--file=somefile.xlsx' if you wish to only test a \n"
	print "                                                 specific file\n\n"
	print "BUILD STRUCTURE ONLY(OPT) '--stuctOnly=true      if you do not want to check links"
	print "Additional Note: The first column of the spreadsheet (the course) \n"
	print "is assumed to match the directory name of the resource files\n"
	abort
end

begin
	ARGV.each do |arg|
	match = /--(?<key>.*?)=(?<value>.*)/.match(arg)
	$args[match[:key]] = match[:value] # e.g. args['first_name'] = 'donald'
	end
	if ($args['bfp'].nil?)
		PrintHelpMessage()
	end
rescue
	PrintHelpMessage()
end
$baseDir = File.join($args['bfp'], "")
if($baseDir.include?("\\"))
	$baseDir = $baseDir.gsub!("\\", "/")
end

if(!File.exists?($baseDir))
	print "invalid base file path\n"
	abort
end
#'C:/Users/jklins/Documents/sandbox/CommonCartridgeLinkChecker/MiddleSchool_B2S_CC/'                                    #VERY IMPORTANT! LOCATION OF SPREADSHEETS
$fileFolderDir = $args['rfp'].nil? ? $baseDir  :  File.join($args['rfp'], "")         #Some cartridges have the files in a sub directory eg. "#{$baseDir}Integrated/"              #                                                    

if($fileFolderDir.include?("\\"))
	$fileFolderDir = $fileFolderDir.gsub!("\\", "/")
end

if(!File.exists?($fileFolderDir))
	print "invalid resource file path\n"
	abort
end

$testFiles = $args['file'].nil? ? '*.xl*' : $args['file']

$structOnly = $args['structOnly'].nil? ? false  :  $args['structOnly'] == "true"? true : false 

##################################################################################################################################
$timeInt = Time.now.to_i
fileoutName = "#{$baseDir}Testing/LinkCheck/allLinksChecked_#{$timeInt}.html" 
fileoutNameNotFound = "#{$baseDir}Testing/LinkCheck/allLinksNotFound_#{$timeInt}.html"

$jquery = "<script src='https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js'></script>"
$buttons = "<div><button id='AllLines'>All</button><button id='NotFound'>Not Found</button><button id='FileMissing'>Missing Files</button><button id='SomethingWentWrong'>Something went wrong</button><div><button id='ErrorDetails'>Show/Hide Error Details</button><div><br/>"
$js = "<script>$('#AllLines').click(function (){$('.all').show();}); $('#NotFound').click(function (){$('.all').hide();$('.notFound').show();}); $('#SomethingWentWrong').click(function (){$('.all').hide();$('.somethingWentWrong').show()});$('#FileMissing').click(function (){$('.all').hide();$('.filemissing').show()});$('#ErrorDetails').click(function (){$('.errorDetails').toggle();});$('.errorDetails').hide()</script>"

def CheckLink(origString, urlString, limit, line, csv, getRequest = false)
	begin	
		url = URI.parse(urlString)
		http = Net::HTTP.new(url.host, url.port)
		if (urlString.index("https") == 0)	
				http.use_ssl = true
				http.verify_mode = OpenSSL::SSL::VERIFY_NONE
		end
		if (getRequest == false) #This if/else is the best I could do to handle redirects and 404s :( ...There is a better way, I'm sure of it!
			request = Net::HTTP::Head.new(url.request_uri) #head is preferred for speed purposes.
			request['Cookie'] = "loggedin=1,teacher=1"
			r = http.request(request)
		else
			r = http.get(url.request_uri)
			cookie = {'Cookie'=>r.to_hash['set-cookie'].collect{|ea|ea[/^.*?;/]}.join}
			r = http.get(url.request_uri,cookie)
		end
	rescue
		p "something went wrong with this request: #{$!.backtrace}  #{line}: #{urlString}"
		$fileoutNotFound.puts "<div class = 'all somethingWentWrong'>something went wrong with this request (rescued): | #{csv} | Line: #{line} |<a href ='#{origString}'>#{origString}</a>| #{urlString} | <div class='errorDetails'>#{$!.backtrace}</div> <br/></div>"
		$brokenLinks = $brokenLinks + 1
		$allbrokenLinks = $allbrokenLinks + 1
		return
	end
	case r
		when Net::HTTPSuccess     then 
			print "#{line}, "
		when Net::HTTPRedirection then 
			#p "redirected  Line: #{line} #{origString}- > #{r['location']}"
			if((r['location'] == '/BIM/login') and (limit > 0))
				CheckLink(origString, origString, limit - 1, line, csv, true)
			else
				CheckLink(origString, r['location'], limit - 1, line, csv, true)
			end
		when Net::HTTPNotFound
			$brokenLinks = $brokenLinks + 1
			$allbrokenLinks = $allbrokenLinks + 1
			$fileoutNotFound.puts "<div class='all notFound'>failed checking -NOT FOUND - (#{r.code}) | #{csv} | Line: #{line} |<a href ='#{origString}'>#{origString}</a>| |<a href =#{urlString}>#{urlString}</a><br/></div>"
			p "failed checking -NOT FOUND - (#{r.code})- Line: #{line} | #{origString}"
		else
		$brokenLinks = $brokenLinks + 1
		$allbrokenLinks = $allbrokenLinks + 1
		$fileoutNotFound.puts "<div class = 'all somethingWentWrong'>failed checking (#{r.code})- | #{csv} | Line: #{line} |<a href ='#{origString}'>#{origString}</a>| |<a href =#{urlString}>#{urlString}</a><br/></div>"
	end
end
 

def buildStructure(item, role) 
	p "Building structure for: #{item}, #{role}"
	contentType = Hash.new();
	chapters = 0
	sections = 0
	i = -1
	book = Array.new()
	#note: if using the June 2015 excel spreadsheets, the line breaks in the headers must first be removed
	$currentChap = ""
	$currentBook = ""
	$counter = 0
	$fileoutName = "#{$baseDir}Testing/Structure/#{item.split('/')[-1].split('.')[0]}.html"
	$fileout2 = File.open($fileoutName, "w")
	CSV.foreach(item) do |currentRow|
		i += 1
		if i == 0 
			next
		end
		cType = currentRow[9].nil? ? "unknownType" : currentRow[9].gsub(/\s+/, "_").downcase.delete("( ):/!\t").to_sym
		if (contentType[cType].nil?)
			contentType[cType] = 1
		else 
			contentType[cType] = contentType[cType] + 1
		end
		book.push(Entry.new(currentRow[0],currentRow[1],currentRow[2].to_i == 0 ? currentRow[2].to_s : "%02d" % currentRow[2].to_i.to_s,currentRow[3].nil? ? "Book Level" : currentRow[3],(currentRow[4].nil? ? "Chapter Level" : currentRow[4]),currentRow[5],currentRow[6].nil? ? "Chapter Level" : currentRow[6],currentRow[7],currentRow[8],(currentRow[9].nil? ? "unknownType" : currentRow[9]),currentRow[14]))
	end
	newbook = book.sort_by{|e| [e.chapTitle[4] == ":" ? e.chapTitle.insert(3, '0') : e.chapTitle, e.sectionTitle, e.resourceType]}
	holdEntry = Entry.new
	j = -1
	$fileout2.puts "<!DOCTYPE html><head><title>#{item} - Structure </title>#{$jquery}</head><body><h1>#{item} - Structure</h1><ul>"
	newbook.each do |b|
		j += 1
		if b.chapTitle == "Book Level" and b.resourceType == "" and b.resourcePath == ""
			next
		end
		if b.chapTitle == "Book Level"
			$fileout2.puts "<li class='all #{b.resourceType.gsub(/\s+/, "_").downcase.delete("( ):/!\t").to_sym}'><span style =\"color:green\">#{b.resourceType}</span> -- <span style=\"color:orange\">#{b.uniqueTitle}</span> -- <a href = \"#{b.resourcePath}\">#{b.resourcePath}</a></li>"
			next
		end
		if b.chapTitle == holdEntry.chapTitle
			if b.sectionTitle == holdEntry.sectionTitle
				$fileout2.puts "<li class='all #{b.resourceType.gsub(/\s+/, "_").downcase.delete("( ):/!\t").to_sym}'><span style =\"color:green\">#{b.resourceType}</span> -- <span style=\"color:orange\">#{b.uniqueTitle}</span> -- <a href = \"#{b.resourcePath}\">#{b.resourcePath}</a></li>"
			else
				sections = sections+1
				$fileout2.puts "</ul></ul>"
				$fileout2.puts "<ul><li><h3>#{b.section} : <span style='color:purple'>#{b.sectionTitle}</span> : <span style='color:#979695'>#{b.standards}</span><h3></li><ul>"
				$fileout2.puts "<li class='all #{b.resourceType.gsub(/\s+/, "_").downcase.delete("( ):/!\t").to_sym}'><span style =\"color:green\">#{b.resourceType}</span> -- <span style=\"color:orange\">#{b.uniqueTitle}</span> -- <a href = \"#{b.resourcePath}\">#{b.resourcePath}</a></li>"
			end
		else
			if j != 0
			$fileout2.puts "</ul></ul>"
			end
			chapters = chapters+1
			sections = sections+1
			$fileout2.puts "<li><h2>#{b.chapter} : #{b.chapTitle}</h2></li>"
			$fileout2.puts "<ul><li><h3>#{b.section} : <span style='color:purple'>#{b.sectionTitle}</span> : <span style='color:#979695'>#{b.standards}</span></h3><ul><li class='all #{b.resourceType.gsub(/\s+/, "_").downcase.delete("( ):/!\t").to_sym}'><span style =\"color:green\">#{b.resourceType}</span> -- <span style=\"color:orange\">#{b.uniqueTitle}</span> -- <a href = \"#{b.resourcePath}\">#{b.resourcePath}</a></li>"
		end
		holdEntry = b
	end
	contentJs="$('#ALL').click(function(){$('.all').show();});\r\n$('#STRUCTUREONLY').click(function(){$('.all').hide();});\r\n"
	buttons = "<button id='ALL'>all</button><button id='STRUCTUREONLY'>Show Structure Only</button>"
	contentType.each do |k,v|
		contentJs = "#{contentJs} $('##{k.upcase}').click(function(){$('.all').hide();$('.#{k}').show();});\r\n"
		buttons = "#{buttons} <button id='#{k.upcase}'>#{k} (#{v})</button>"
	end
	buttons = "<h2>Number of Chapters: #{chapters}</h2><h2>Number of Sections: #{sections}</h2> #{buttons}"
	contentJs = "<script>  $( document ).ready(function() { $('body').prepend(\"#{buttons}\"); #{contentJs}});</script>"
	$fileout2.puts "</ul></li></ul>#{contentJs}</body></html>"  
	$fileout2.close
end

def DoCSVLinkStuff(csv, startLine)
	p "Checking links with: #{csv}"
	checkedLinks = 0
	firstLine = true
	CSV.foreach(csv) do |row| #Note if using csv from database, quotes needed removed from some rows
		if (firstLine == true)
			firstLine = false
			next
		end
		checkedLinks = checkedLinks + 1
		$allLinks = $allLinks + 1
		webLink = false
		if((row[8].to_s.empty?) or (row[0].to_s.empty?))
			$fileoutNotFound.puts "empty line:| #{csv} | Line: #{checkedLinks}"
			next
		end
		if (row[8].index("https") == 0) #If it's a webLink or if it's a file. Weblinks for cartidges must start with https, not http or //
			fullUrl = row[8]
			webLink = true
		else
			fullUrl = "#{$fileFolderDir}#{row[0].capitalize}/#{row[8]}" #get file full path for checking
		end
		if checkedLinks < startLine
			next
		end
		$fileout.puts "Line: #{checkedLinks}: <a href ='#{fullUrl}'>#{fullUrl}</a><br/>"
		if(webLink == true)
			begin
				CheckLink(fullUrl, fullUrl, 10, checkedLinks, csv)
			rescue
				$brokenLinks = $brokenLinks + 1
				$allbrokenLinks = $allbrokenLinks + 1
				p "failed checking (rescued)- #{csv} Line: #{checkedLinks}: | <a href ='#{fullUrl}'>#{fullUrl}</a><br/>"
				$fileoutNotFound.puts "<div class = 'all somethingWentWrong'>failed checking (rescued): <div class='errorDetails'>#{$!.backtrace}</div>  | #{csv} | Line: #{checkedLinks} |<a href ='#{fullUrl}'>#{fullUrl}</a>|<br/></div>"
			end
		else
			print "#{checkedLinks}, "
			if(File.file?(fullUrl))
				#found the file
			else
				$allbrokenLinks = $allbrokenLinks + 1
				p "#{csv} Line: #{checkedLinks}: | #{fullUrl}"
				$fileoutNotFound.puts "<div class = 'all filemissing'>failed checking FILE: | #{csv} | Line: #{checkedLinks} |<a href ='#{fullUrl}'>#{fullUrl}</a>|<br/></div>"
			end
		end
	end
end

def CreateCSVsAndBuildStructuresTestLinks(spreadSheetFile, sheetNum, role)
	workbook = Roo::Spreadsheet.open(spreadSheetFile)
	tempCsvName = "#{$baseDir}Testing/Csv/#{spreadSheetFile.split('/')[-1].split('.')[0]}_#{role}_#{$timeInt}.csv"
	tempCsv = File.open(tempCsvName, "w")
	tempCsv.puts(workbook.sheet(sheetNum).to_csv)
	tempCsv.close
	buildStructure(tempCsvName, role)
	if $structOnly == false
		DoCSVLinkStuff(tempCsvName, 0)
	end
end

def CreateDirectories()
	directory_name = "#{$baseDir}Testing"
	Dir.mkdir(directory_name) unless File.exists?(directory_name)
	directory_name = "#{$baseDir}Testing/Structure"
	Dir.mkdir(directory_name) unless File.exists?(directory_name)
	directory_name = "#{$baseDir}Testing/Csv"
	Dir.mkdir(directory_name) unless File.exists?(directory_name)
	directory_name = "#{$baseDir}Testing/LinkCheck"
	Dir.mkdir(directory_name) unless File.exists?(directory_name)
end
 
def main(fileoutName, fileoutNameNotFound)
	checkedLinks = 0
	webLink = false
	firstRound = true
	Dir.glob("#{$baseDir}#{$testFiles}") do |sSheet|
		if (sSheet.include?("~")) #assumes these are temporary files created when the workbook is opened by a user
			next
		end
		p "Starting: #{sSheet}"
		if(firstRound == true)
			CreateDirectories()
			if $structOnly == false
				$fileout = File.open(fileoutName, "w")
				$fileout.puts "<!DOCTYPE html><html><head><title>Common Cartridges all Links</title></head><body>"
				$fileoutNotFound = File.open(fileoutNameNotFound, "w")
				$fileoutNotFound.puts "<!DOCTYPE html><html><head><title>Common Cartridges all Links Not Found</title>#{$jquery}</head><body>#{$buttons}"
			end
			firstRound = false
		end
		if $structOnly == false
			$fileout.puts "<h1>#{sSheet}</h1>"
			$fileoutNotFound.puts "<h1>#{sSheet}</h1>"
		end
		CreateCSVsAndBuildStructuresTestLinks(sSheet, 0, "Teacher")
		CreateCSVsAndBuildStructuresTestLinks(sSheet, 1, "Student")
	end

	if firstRound == true
		print "no spreadsheet files found to work with\n"
		abort
	end
	if $structOnly == false
		$fileout.puts "<h2>Total Checked Links: #{$allLinks}</h2>"
		$fileout.puts "<h2>Total Broken Links: #{$allbrokenLinks}</h2>"
		$fileout.puts "See <a href = 'AllCartridgeLinksNotFound.html'>AllCartridgeLinksNotFound.html</a> For Specific Info Related to Links without 200 code returns"
		$fileout.puts "</body></html>"
		p "Total Checked Links: #{$allLinks}" 
		p "File with all links: #{fileoutName}"
		p "Total Broken Links: #{$allbrokenLinks} "
		p "File with broken links: #{fileoutNameNotFound}"
		$fileout.close
		$fileoutNotFound.puts "<h2 style='color:blue'>Total Checked Links: #{$allLinks}</h2>"
		$fileoutNotFound.puts "<h2 style='color:red'>Total Broken Links: #{$allbrokenLinks}</h2>"
		$fileoutNotFound.puts "#{$js}</body></html>"
		$fileoutNotFound.close
	end
end 

main(fileoutName, fileoutNameNotFound)