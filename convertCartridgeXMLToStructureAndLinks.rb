begin
  require 'rubygems'
rescue LoadError
  puts "Required gem rubygems not found. Installing now..."
  `gem install rubygems`
  puts "done installing rubygems"
  require 'rubygems'
end
begin
  require 'xmlsimple'
rescue LoadError
  puts "Required gem xmlsimple not found. Installing now..."
  `gem install xmlsimple`
  puts "done installing xmlsimple"
  require 'xmlsimple'
end
begin
  require 'net/https' #use https or http depending on use- also see code below
rescue LoadError
  puts "Required gem net/https not found. Installing now..."
  `gem install net/https`
  puts "done installing net/https"
  require 'net/https'
end

class CartridgeResouce
	def initialize(book, chapter, section, resource, resourceRef, type, link, found, sortOrder)
		@book = book
		@chapter = chapter
		@section = section
		@resource = resource
		@resourceRef = resourceRef
		@link = link
		@type = type
		@found = found
		@sortOrder = sortOrder
	end
	attr_reader :book
	attr_writer :book
	attr_reader :chapter
	attr_writer :chapter
	attr_reader :section
	attr_writer :section
	attr_reader :resource
	attr_writer :resource
	attr_reader :resourceRef
	attr_writer :resourceRef
	attr_reader :link
	attr_writer :link
	attr_reader :type
	attr_writer :type
	attr_reader :found
	attr_writer :found
	attr_reader :sortOrder
	attr_writer :sortOrder
end

$cartridgeResources = []
$args = {}
$brokenLinks = 0
$allbrokenLinks = 0
$timeInt = Time.now.to_i
$jquery = "<script src='https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js'></script>"

def GetWebLink(path)
	xml2 = XmlSimple.xml_in(path)
	return xml2["url"][0]["href"]
end

def PrintHelpMessage()
	print "\nplease run the script with the following options:\n"
	print "BASE FILE PATH: (REQ):    '--bfp=base/File/Path/' this is the location of\n"
	print "                                                  the cartridge spreadsheets\n\n"
	print "FILENAME (OPT):           '--file=somefile.xml' if you wish to only test a \n"
	print "                                                 specific file\n\n"
	print "BUILD STRUCTURE ONLY(OPT) '--stuctOnly=true      if you do not want to check links"
	abort
end

begin
	ARGV.each do |arg|
	match = /--(?<key>.*?)=(?<value>.*)/.match(arg)
	$args[match[:key]] = match[:value] # e.g. args['first_name'] = 'donald'
	end
	if ($args['bfp'].nil?)
		PrintHelpMessage()
	end
rescue
	PrintHelpMessage()
end
$baseDir = File.join($args['bfp'], "")
if($baseDir.include?("\\"))
	$baseDir = $baseDir.gsub!("\\", "/")
end

if(!File.exists?($baseDir))
	print "invalid base file path\n"
	abort
end

$testFiles = $args['file'].nil? ? '*.xml*' : $args['file']
$structOnly = $args['structOnly'].nil? ? false  :  $args['structOnly'] == "true"? true : false 

def CreateDirectories()
	directory_name = "#{$baseDir}Testing"
	Dir.mkdir(directory_name) unless File.exists?(directory_name)
end

def CreateFiles()
	CreateDirectories()
	p "Building structure"
	contentType = Hash.new();
	chapters = 0
	sections = 0
	i = -1
	$currentChap = ""
	$currentBook = ""
	$counter = 0
	fname = $cartridgeResources[0].book.gsub(/\s+/, "_").downcase.delete("( ).:/!\t").to_sym
	$fileoutName = "#{$baseDir}Testing/#{fname==""? "cartridgeTest" : fname}_#{$timeInt}.html"
	$fileout2 = File.open($fileoutName, "w")
	#(book, chapter, section, resource, resourceRef, type, link, found)	

	newbook = $cartridgeResources.sort_by{|e| [e.sortOrder, e.book, e.chapter, e.section, e.resource]}
	holdEntry = CartridgeResouce.new(nil,nil,nil,nil,nil,nil,nil,nil,nil)
	j = -1
	contentType = Hash.new();
	$fileout2.puts "<!DOCTYPE html><head><title>#{$baseDir} - Structure </title>#{$jquery}</head><body><h1>#{$baseDir} - Structure</h1><ul>"
	newbook.each do |b|
		begin
			cType = b.resource.nil? ? "unknownType" : b.resource.gsub(/\s+/, "_").downcase.delete("( ).:/!\t").to_sym
		rescue
			$fileout2.puts"<li class='all rescued #{b.type} #{b.found}'><span style =\"color:green\">#{b.resource}</span> -- <span style=\"color:orange\">Verified? #{b.found}</span> -- <a href = \"#{b.link}\">#{b.link}</a><span style=\"color:grey\" class='resourceRef'> -- ResourceRef: #{b.resourceRef}</span></li>"
			next
		end
		if (contentType[cType].nil?)
			contentType[cType] = 1
		else 
			contentType[cType] = contentType[cType] + 1
		end
		
		j += 1
		if b.chapter == 0 and b.resource == "" and b.link == ""
			next
		end
		if b.chapter == 0
			$fileout2.puts "<li class='all #{b.resource.gsub(/\s+/, "_").downcase.delete("( ).:/!\t").to_sym} #{b.type} #{b.found}'><span style =\"color:green\">#{b.resource}</span> -- <span style=\"color:orange\">Verified? #{b.found}</span> -- <a href = \"#{b.link}\">#{b.link}</a><span style=\"color:grey\" class='resourceRef'> -- ResourceRef: #{b.resourceRef}</span></li>"
			next
		end
		if b.chapter == holdEntry.chapter
			if b.section == holdEntry.section
				$fileout2.puts "<li class='all #{b.resource.gsub(/\s+/, "_").downcase.delete("( ).:/!\t").to_sym} #{b.type} #{b.found}'><span style =\"color:green\">#{b.resource}</span> -- <span style=\"color:orange\">Verified? #{b.found}</span> -- <a href = \"#{b.link}\">#{b.link}</a><span style=\"color:grey\" class='resourceRef'> -- ResourceRef: #{b.resourceRef}</span></li>"
			else
				sections = sections+1
				$fileout2.puts "</ul></ul>"
				$fileout2.puts "<ul><li><h3>#{b.section}<h3></li><ul>"
				$fileout2.puts "<li class='all #{b.resource.gsub(/\s+/, "_").downcase.delete("( ).:/!\t").to_sym} #{b.type} #{b.found}'><span style =\"color:green\">#{b.resource}</span> -- <span style=\"color:orange\">Verified? #{b.found}</span> -- <a href = \"#{b.link}\">#{b.link}</a><span style=\"color:grey\" class='resourceRef'> -- ResourceRef: #{b.resourceRef}</span></li>"
			end
		else
			if j != 0
			$fileout2.puts "</ul></ul>"
			end
			ch = b.chapter
			if(ch == "")
				ch = "Book Level"
			else
				chapters = chapters+1
			end
			$fileout2.puts "<li><h2>#{ch}</h2></li>"
			sec = b.section
			if(sec == "")
				sec = "Chapter Level"
			else
				sections = sections+1
			end
			$fileout2.puts "<ul><li><h3>#{sec}</h3><ul><li class='all #{b.resource.gsub(/\s+/, "_").downcase.delete("( ).:/!\t").to_sym} #{b.type} #{b.found}'><span style =\"color:green\">#{b.resource}</span> -- <span style=\"color:orange\">Verified? #{b.found}</span> -- <a href = \"#{b.link}\">#{b.link}</a><span style=\"color:grey\" class='resourceRef'> -- ResourceRef: #{b.resourceRef}</span></li>"
		end
		holdEntry = b.clone
	end
	contentJs="$('#ALL').click(function(){$('.all').show();});\r\n$('#Found').click(function(){$('.all').hide(); $('.true').show();})\r\n$('#NotFound').click(function(){$('.all').hide(); $('.false').show();})\r\n$('#ShowStructureOnly').click(function(){$('.all').hide();})\r\n$('#ShowResourceRef').click(function(){$('.resourceRef').toggle();})\r\n$('.resourceRef').hide();\r\n$('#Rescued').click(function(){$('.all).hide();$('.rescued').show();})\r\n"
	buttons = "<button id='ALL'>all</button><button id='Found'>Found</button><button id='NotFound'>Not Found</button><button id='ShowStructureOnly'>Show Structure Only</button><button id='ShowResourceRef'>Show/Hide Resource Ref</button><button id='Rescued'>Rescued</button>"
	#contentType.each do |k,v|
	#	contentJs = "#{contentJs} $('##{k.upcase}').click(function(){$('.all').hide();$('.#{k}').show();});\r\n"
	#	buttons = "#{buttons} <button id='#{k.upcase}'>#{k} (#{v})</button>"
	#end
	buttons = "<h2>Number of Chapters: #{chapters}</h2><h2>Number of Sections: #{sections}</h2> #{buttons}"
	contentJs = "<script>  $( document ).ready(function() { $('body').prepend(\"#{buttons}\"); #{contentJs}});</script>"
	$fileout2.puts "</ul></li></ul>#{contentJs}</body></html>"  
	$fileout2.close
end

def CheckLink(linkInfo, origString, urlString, line, limit, getRequest = false)
	begin	
		url = URI.parse(urlString)
		http = Net::HTTP.new(url.host, url.port)
		if (urlString.index("https") == 0)	
				http.use_ssl = true
				http.verify_mode = OpenSSL::SSL::VERIFY_NONE
		end
		if (getRequest == false) #This if/else is the best I could do to handle redirects and 404s :( ...There is a better way, I'm sure of it!
			request = Net::HTTP::Head.new(url.request_uri)
			request['Cookie'] = "loggedin=1,teacher=1"
			r = http.request(request)
		else
			r = http.get(url.request_uri)
			cookie = {'Cookie'=>r.to_hash['set-cookie'].collect{|ea|ea[/^.*?;/]}.join}
			r = http.get(url.request_uri,cookie)
		end
	rescue
		p "something went wrong with this request: #{$!.backtrace}  #{line}: #{urlString}"
		#fileoutNotFound.puts "<div class='all somethingWentWrong'>something went wrong with this request (rescued): | Line: #{line} |#{linkInfo}|<a href ='#{origString}'>#{origString}</a>| #{urlString} | #{$!.backtrace} <br/></div>"
		$brokenLinks = $brokenLinks + 1
		$allbrokenLinks = $allbrokenLinks + 1
		return false 
	end
	case r
		when Net::HTTPSuccess     then 
			print "#{line}, "
			return true
		when Net::HTTPRedirection then 
			#p "redirected  Line: #{line} #{origString}- > #{r['location']}"
			if((r['location'] == '/BIM/login') and (limit > 0))
				return CheckLink(linkInfo, origString, origString, line, limit - 1,  true)
			else
				return CheckLink(linkInfo, origString, r['location'], line, limit - 1,  true)
			end
		when Net::HTTPNotFound
			$brokenLinks = $brokenLinks + 1
			$allbrokenLinks = $allbrokenLinks + 1
			p "failed checking -NOT FOUND - (#{r.code})- Line: #{line} | #{origString}"
			return false
		else
		$brokenLinks = $brokenLinks + 1
		$allbrokenLinks = $allbrokenLinks + 1
		return false
		#fileoutNotFound.puts "<div class='all somethingWentWrong'>failed checking (#{r.code})- | Line: #{line} |#{linkInfo}|<a href ='#{origString}'>#{origString}</a>| |<a href =#{urlString}>#{urlString}</a><<br/></div>"
	end
end

def CheckCartridgeLinksAndCreateFiles()
  if $structOnly == true
	CreateFiles()
	return
  end
  $cartridgeResources.each_with_index do |res, index|
	if (res.type == "file")
		print "#{index}, "
		if(File.file?(res.link))
				$cartridgeResources[index].found = true
		else
			$cartridgeResources[index].found = false
			$allbrokenLinks = $allbrokenLinks + 1
			p "Missing File: #{res.link}"
		end
	else
		$cartridgeResources[index].found = CheckLink("#{res.book}: #{res.chapter}: #{res.section}: #{res.resource}", res.link, res.link, index, 10, false)
	end
  end
  CreateFiles()
end

Dir.glob("#{$baseDir}#{$testFiles}") do |item|
  myXml = XmlSimple.xml_in(item)
 #class CartridgeResouce   def initialize(book, chapter, section, resource, resourceRef, link)
	i = 0
	myXml["organizations"].each do |orgs|
		orgs["organization"].each do |org|
			#p org["title"]
			org["item"].each do |book|
				book["item"].each do |chapter| #Chapter Level
					i = i + 1
					#p chapter["title"]
					if(chapter["item"].nil?) 
						#p "chapter: #{chapter["title"]}: #{chapter["identifierref"]}" 
						$cartridgeResources.push(CartridgeResouce.new(org["title"][0], "", "", chapter["title"][0], chapter["identifierref"], nil, nil,false , i))
						next 
					end
					chapter["item"].each do |resource|
						#p "section: #{resource["title"]}: "
						if(resource["identifierref"].nil?)
							#$k = $k+1
							resource["item"].each do |itm|
								#p "item: #{itm["title"]}: #{itm["identifierref"]}"
								$cartridgeResources.push(CartridgeResouce.new(org["title"][0], chapter["title"][0], resource["title"][0], itm["title"][0], itm["identifierref"], nil, nil,false , i))
							end
						else
							#p "section: #{resource["title"]} #{resource["identifierref"]}"
							$cartridgeResources.push(CartridgeResouce.new(org["title"][0], chapter["title"][0], "", resource["title"][0], resource["identifierref"], nil, nil, false , i))
						end
					end
				end
			end
		end 
	end
	myXml["resources"].each do |ress|
		ress["resource"].each do |res| #resource
			idx = $cartridgeResources.find_index {|item| item.resourceRef == res["identifier"]}
			if (res["type"] == "webcontent")
				$cartridgeResources[idx].type = "file" #I know this sounds backwards.
				$cartridgeResources[idx].link = "#{$baseDir}#{res["file"][0]["href"]}"
			else
				$cartridgeResources[idx].type = "link" 
				$cartridgeResources[idx].link = GetWebLink("#{$baseDir}#{res["file"][0]["href"]}")
			end
		end
	end	
end
CheckCartridgeLinksAndCreateFiles()


 
